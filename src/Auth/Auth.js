// Import Stack
import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { observer } from 'controllerim'

// Import Controllers & Styles
import { AuthController } from './AuthController'
import styles from './AuthStyles'

// View
export const Auth = observer(class extends Component {

  componentWillMount() {
    this.controller = new AuthController( this )
  }

  render() {
    return (
      <View>
        <Text>Auth</Text>
      </View>
    )
  }

})
