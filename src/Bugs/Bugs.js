// Import Stack
import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { observer } from 'controllerim'

// Import Controllers & Styles
import { BugsController } from './BugsController'
import styles from './BugsStyles'

// View
export const Bugs = observer(class extends Component {

  componentWillMount() {
    this.controller = new BugsController( this )
  }

  render() {
    return (
      <View>
        <Text>Bugs</Text>
      </View>
    )
  }

})
