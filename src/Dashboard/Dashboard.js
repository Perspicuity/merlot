// Import Stack
import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { observer } from 'controllerim'

// Import Controllers & Styles
import { DashboardController } from './DashboardController'
import styles from './DashboardStyles'

// View
export const Dashboard = observer(class extends Component {

  componentWillMount() {
    this.controller = new DashboardController( this )
  }

  render() {
    return (
      <View>
        <Text>Dashboard</Text>
      </View>
    )
  }

})
