// Import Stack
import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { observer } from 'controllerim'

// Import Controllers & Styles
import { InformationController } from './InformationController'
import styles from './InformationStyles'

// View
export const Information = observer(class extends Component {

  componentWillMount() {
    this.controller = new InformationController( this )
  }

  render() {
    return (
      <View>
        <Text>Information</Text>
      </View>
    )
  }

})
