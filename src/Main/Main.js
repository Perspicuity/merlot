// Import Stack
import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { observer } from 'controllerim'
import { createStackNavigator } from 'react-navigation'

// Import Controllers & Styles
import { MainController } from './MainController'
import styles from './MainStyles'

// View
export const Main = observer(class extends Component {

  componentWillMount() {
    this.controller = new MainController( this )
  }

  render() {
    return (
      <View>
        <Text>Main</Text>
      </View>
    )
  }

})
