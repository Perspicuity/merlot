import { Controller } from 'controllerim'
import firebase from 'react-native-firebase'

export class MainController extends Controller {

  constructor(comp) {
    super (comp)
    this.state = {
      componentIsLoading: false,
      projects: []
    }
  }


  // Data
  async fetchProjects() {
    this.setComponentIsLoading(true)
    var ref = firebase.firestore().collection('Gyms')
    try {
      var snapshot = await ref.get()
      snapshot.forEach(doc => {
        var obj = doc.data()
        obj.gid = doc.id
        this.state.gyms.push(obj)
      })
      this.setComponentIsLoading(false)
    }
    catch (err) {
      console.log('error ', err);
    }
  }
  getProjects() {
    return this.state.projects
  }


  // Map
  getComponentIsLoading() {
    return this.state.componentIsLoading
  }

  setComponentIsLoading(bool) {
    this.state.componentIsLoading = bool
  }

}
