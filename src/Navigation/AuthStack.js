import { createSwitchNavigator, createStackNavigator } from 'react-navigation';

// Implementation of HomeScreen, OtherScreen, SignInScreen, AuthLoadingScreen
// goes here.

import SplashScreen from '../Splash/Splash'
import TestOne from '../Test/TestOne'
import TestTwo from '../Test/TestTwo'

export default createSwitchNavigator(
  {
    Splash: SplashScreen,
    App: TestOne,
    Auth: TestTwo,
  },
  {
    initialRouteName: 'Splash',
  }
);
