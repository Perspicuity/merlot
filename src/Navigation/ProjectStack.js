// Import Stack
import React, { Component } from 'react'
import { createStackNavigator } from 'react-navigation'

// Import Views
import { Projects } from '../Projects/Projects'
import ProjectTabs from './ProjectTabs'

export default createStackNavigator({

  Projects: Projects,
  ProjectTabs: ProjectTabs

}, {
  initialRouteName: 'Projects'
})
