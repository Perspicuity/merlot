// Import Stack
import React, { Component } from 'react'
import { createBottomTabNavigator } from 'react-navigation'

// Import Views
import { Dashboard } from '../Dashboard/Dashboard'
import { Tasks } from '../Tasks/Tasks'
import { Bugs } from '../Bugs/Bugs'
import { Information } from '../Information/Information'

export default createBottomTabNavigator({

  Dashboard: Dashboard,
  Tasks: Tasks,
  Bugs: Bugs,
  Information: Information

}, {
  initialRouteName: 'Dashboard'
})
