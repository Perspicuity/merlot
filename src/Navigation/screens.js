import { Navigation } from 'react-native-navigation';

import Splash from '../Splash/Splash';
import TestOne from '../Test/TestOne';
import TestTwo from '../Test/TestTwo';

// register all screens of the app (including internal ones)
export function registerScreens() {
  Navigation.registerComponent('merlot.Splash', () => Splash);
  Navigation.registerComponent('merlot.TestOne', () => TestOne);
  Navigation.registerComponent('merlot.TestTwo', () => TestTwo);
}
