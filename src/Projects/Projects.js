// Import Stack
import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { observer } from 'controllerim'

// Import Controllers & Styles
import { ProjectsController } from './ProjectsController'
import styles from './ProjectsStyles'

// View
export const Projects = observer(class extends Component {

  componentWillMount() {
    this.controller = new ProjectsController( this )
  }

  render() {
    return (
      <View>
        <Text>Projects</Text>
      </View>
    )
  }

})
