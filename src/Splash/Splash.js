import React, { Component } from 'react';
import { Text, View, ActivityIndicator } from 'react-native';

import { observer } from 'controllerim'

import { SplashController } from './SplashController'

class Splash extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isAuthenticated: true
    }
  }

  componentWillMount() {
    this.controller = new SplashController(this)
  }

  componentDidMount() {
    this.props.navigation.navigate('App');
  }

  render() {
    return (
      <View style={{flex: 1, justifyContent: 'center'}}>
        <ActivityIndicator size="large" color="#000000" />
      </View>
    );
  }
}

export default observer(Splash)
