// Import Stack
import React, { Component } from 'react'
import { View, Text, ActivityIndicator } from 'react-native'
import { observer } from 'controllerim'

// Import Controllers & Styles
import { SplashController } from './SplashController'
import styles from './SplashStyles'

// View
export const Splash = observer(class extends Component {

  componentWillMount() {
    this.controller = new SplashController( this )
  }

  componentDidMount() {
    console.log(this.props.navigation);
    this.props.navigation.navigate('Auth')
  }


  render() {
    return (
      <View style={{flex: 1, justifyContent: 'center'}}>
        <ActivityIndicator size="large" color="#000000" />
      </View>
    )
  }

})
