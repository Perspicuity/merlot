import { Controller } from 'controllerim'
import firebase from 'react-native-firebase'

export class SplashController extends Controller {

  constructor(comp) {
    super (comp)
    this.state = {
      isAuthenticated: false
    }
  }

  isAuthenticated() {
    return this.state.isAuthenticated
  }

}
