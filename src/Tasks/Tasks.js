// Import Stack
import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { observer } from 'controllerim'

// Import Controllers & Styles
import { TasksController } from './TasksController'
import styles from './TasksStyles'

// View
export const Tasks = observer(class extends Component {

  componentWillMount() {
    this.controller = new TasksController( this )
  }

  render() {
    return (
      <View>
        <Text>Tasks</Text>
      </View>
    )
  }

})
