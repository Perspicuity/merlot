import React, { Component } from 'react';
import { Text, View } from 'react-native';

class TestOne extends Component {
  render() {
    return (
      <View>
        <Text>
          TestOne
        </Text>
      </View>
    );
  }
}

export default TestOne
